<?php
    // Register nav walker class
    require_once('wp_bootstrap_navwalker.php');
    add_action( 'wp_enqueue_scripts', 'mat_assets' );
    add_theme_support( 'post-thumbnails' );
    $features = array (
        'gallery'
    );
    add_theme_support( 'post-formats', $features );
    
    function mat_assets() {
        wp_enqueue_style( 'lovecraft-blog', get_stylesheet_uri() );
    }

    add_filter( 'wpcf7_form_elements', 'mycustom_wpcf7_form_elements' );

    function mycustom_wpcf7_form_elements( $form ) {
    $form = do_shortcode( $form );

    return $form;
    }


    function register_my_menus() {
      register_nav_menus(
        array(
          'main-menu' => __( 'Main menu' ),
          'footer-menu' => __( 'Footer Menu' )
        )
      );
    }
    add_action( 'after_setup_theme', 'register_my_menus' );

    function get_cat_post($cat, $page) {
        $args = array (
            'category_name' => $cat,
            'posts_per_page' => $page
        );
        $the_query = new WP_Query ($args);

        if ($the_query->have_posts()) {
            while($the_query->have_posts()) {
                $the_query->the_post();
                get_template_part( 'template-parts/content', '');
            }
            wp_reset_postdata(); 
        } else {
            get_template_part('template-parts/content', 'none' );
        }
    }

    function get_simple_post($cat, $page) {
        $args = array (
            'category_name' => $cat,
            'posts_per_page' => $page
        );
        $the_query = new WP_Query ($args);

        if ($the_query->have_posts()) {
            while($the_query->have_posts()) {
                $the_query->the_post();
                get_template_part( 'template-parts/content', 'simple');
            }
            wp_reset_postdata(); 
        } else {
            get_template_part('template-parts/content', 'none' );
        }
    }

    function get_page_post($page) {
        $args = array (
            'pagename' => $page,
        );
        $the_query = new WP_Query ($args);

        if ($the_query->have_posts()) {
            while($the_query->have_posts()) {
                $the_query->the_post();
                get_template_part( 'template-parts/content', '');
            }
            wp_reset_postdata(); 
        } else {
            get_template_part('template-parts/content', 'none' );
        }
    }

    function get_homepage_post($page) {
        $args = array (
            'pagename' => $page,
        );
        $the_query = new WP_Query ($args);

        if ($the_query->have_posts()) {
            while($the_query->have_posts()) {
                $the_query->the_post();
                get_template_part( 'template-parts/content', 'home');
            }
            wp_reset_postdata(); 
        } else {
            get_template_part('template-parts/content', 'none' );
        }
    }

    /**
    * Allow shortcodes in Contact Form 7
    */
    function shortcodes_in_cf7( $form ) {
        $form = do_shortcode( $form );
        return $form;
    }
    add_filter( 'wpcf7_form_elements', 'shortcodes_in_cf7' );



