<?php /* Template Name: home */ ?>
<?php 	get_header( null ); ?>

	<?php get_homepage_post('home') ?>
	<div class="main--container container">
		<div class="main--media">
			<div class="col-xs-12 main--media-header">
				<h2>The Author</h2>
			</div>
			
			<div class="row">
				<?php 
					// Gets last 3 categories post by story
					get_simple_post('who', 3);
				?>
			</div>
			<!-- TODO: Open up a page for learning more about the author -->
			<!-- <a href=""><h3>Learn more ></h3></a> -->
		</div>
		
		<hr>

		<div class="main--why">
			<div class="col-xs-12 main--why-header">
				<h2>Why Lovecraft?</h2>
			</div>
			
			<div class="row">
				<?php 
					// Gets last 3 categories post by story
					get_simple_post('benefits', 3);
				?>
			</div>
			<a href="/category/benefits/"><h3>See more amazing benefits ></h3></a>
		</div>
		
		<hr>
		
		<!-- <div class="main--peers">
			<div class="col-xs-12 main--peers-header">
				<h2>The People Have Spoken</h2>
			</div>
			
			<div class="row">
			</div>

			<a href=""><h3>See more testimonials ></h3></a>
		</div>

		<hr> -->

		<div class="main--stories">
			<div class="col-xs-12 main--stories-header">
				<h2>Recent Stories</h2>
				<h3>Lovecraft's stories are what keep the mythos alive and expanding everyday</h3>
			</div>
			
			<div class="row">
				<?php 
					// Gets last 3 categories post by story
					get_cat_post('stories', 6);
				?>
			</div>
			<a href="category/stories"><h3>Discover more stories ></h3></a>
		</div>
		
	</div>
<?php get_footer(	); ?>