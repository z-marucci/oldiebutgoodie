<?php if (is_single( '' )): ?>
	<?php get_template_part( 'template-parts/content', 'single' ); ?>
<?php else: ?>

<div class="content--box col-xs-12 col-md-4 content--item col-lg-6">	
	<h2 class="content--title">
		<a href="<?php the_permalink( $post ); ?>"><?php the_title( '', '', true ); ?></a>
	</h2>

	<div class="content--image">
		<?php if (has_post_thumbnail( null )): ?>
				<a href="<?php the_permalink( $post ); ?>"><?php  the_post_thumbnail('full', ['class' => 'img-responsive']); ?>
				</a>
		<?php endif; ?>
	</div>

	<div class="content--body">
		<?php the_excerpt(); ?>
		<a href="<?php the_permalink( $post ); ?>"> Read on..</a>

		<p>
			<strong>Posted by: <?php the_author(); ?></strong>
		</p>
		<p>
			<strong>Posted on <?php the_date( '', '', '', true ); ?> at: <?php the_time( '' ); ?></strong>
		</p>
	</div>
</div>

<?php endif; ?>