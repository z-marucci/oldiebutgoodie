<div id="myCarousel" class="carousel slide ph--slider" data-ride="carousel">
  <!-- Wrapper for slides -->
      <div class="carousel-caption">
        <h1><?php bloginfo( 'name' ); ?></h1>
        <p class="lead"><?php the_content( null, false ); ?></p>
      </div>
  <div class="ph--slider-overlay"></div>
  <div class="carousel-inner">
    <div class="item active">
      <?php if (has_post_thumbnail( null )): ?>
          <?php  the_post_thumbnail('full', ['class' => 'img-responsive']); ?>
      <?php endif; ?>
        </div>
    </div>
</div>