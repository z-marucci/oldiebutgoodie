<div class="content--box col-xs-12 col-md-4 content--item col-lg-6">	
	<h2 class="content--title">
		<?php the_title( '', '', true ); ?>
	</h2>

	<div class="content--image">
		<?php if (has_post_thumbnail( null )): ?>
				<a href="<?php the_permalink( $post ); ?>"><?php  the_post_thumbnail('full', ['class' => 'img-responsive']); ?>
				</a>
		<?php endif; ?>
	</div>

	<div class="content--body">
		<?php the_excerpt(); ?>
	</div>
</div>