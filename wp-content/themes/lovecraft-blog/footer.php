    <?php wp_footer() ?>

        <div class="main--footer">
            <div class="container">
                <h2>The Lovecraft portal</h2>
                
                <?php
                    wp_nav_menu( array(
                        'menu'              => 'primary',
                        'theme_location'    => 'footer-menu',
                        'container'         => 'div',
                        'container_class'   => 'footer--menu',
                        'container_id'      => 'footer--main',
                    ));
                ?>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
         <!-- Include all compiled plugins (below), or include individual files as needed -->
        <div class="main--copyright container-fluid">
            <p>&copy; <?php bloginfo( 'title' ); ?> - Site developed by Angelo A. Marucci 2017</p>
        </div>
    </body>
</html>