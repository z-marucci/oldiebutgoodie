<?php /* Template Name: simple-page */ ?>
<?php 	get_header( null ); ?>
<div class="simple--page">
	<div class="container">
		<?php 
			if (have_posts()) {
				while (have_posts()) {
					the_post();
					the_title( '<h2 class="simple--title">', '</h2>', true );
					 if (has_post_thumbnail( null )) {
						the_post_thumbnail('full', ['class' => 'img-responsive']);
					} 
					the_content( null, false );
				}
			} else {
				get_template_part('template-parts/content', 'none' );
			}
		?>
	</div>
</div>
<?php get_footer(	); ?>