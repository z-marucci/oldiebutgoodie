var gulp = require ('gulp'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    minify = require('gulp-minify'),
    rename = require('gulp-rename'),
    imagemin = require('gulp-imagemin');

gulp.task('styles', function(){
    return gulp.src('sass/main.scss')
    .pipe(sass())
    .pipe(rename('style.css'))
    .pipe(minify())
    .pipe(gulp.dest(''));
});

gulp.task('imgcrush', () =>
    gulp.src('img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'))
);

gulp.task('watch', function (){
    gulp.watch(['sass/**/*.scss'], ['styles']);
});

gulp.task('default', ['styles', 'watch', 'imgcrush']);
