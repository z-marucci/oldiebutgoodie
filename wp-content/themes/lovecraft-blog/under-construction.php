<?php /* Template Name: under-construction */ ?>
<?php 	get_header( null ); ?>

	<div class="main--container container">
		<div class="main--header">
			<h1 class="main--title">
				<?php bloginfo('name'); ?>
			</h1>
		</div>
		
		<div class="main--stories">
			<div class="col-xs-12 main--stories-header">
				<?php 
					if (have_posts()) {
						while (have_posts()) {
							the_post();
							the_content( null, false );
						}
					} else {
						get_template_part('template-parts/content', 'none' );
					}
				?>
			</div>
		</div>
		
	</div>
<?php get_footer(	); ?>