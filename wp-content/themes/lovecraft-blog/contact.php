<?php /* Template Name: contact-page */ ?>
<?php 	get_header( null ); ?>
<div class="simple--page contact--page">
	<div class="container">

		<div class="content--image">
			<?php if (has_post_thumbnail( null )): ?>
					<?php  the_post_thumbnail('full', ['class' => 'img-responsive']); ?>
			<?php endif; ?>
		</div>

		<h2>Like to draw Lovecraftian related artwork? Want to share it to the world?</h2>
		<p class="lead">Send us a message and we'll see how we can help eachother</p>
		
		<hr>	

		<?php 
			if (have_posts()) {
				while (have_posts()) {
					the_post();
					the_content( null, false );
				}
			} else {
				get_template_part('template-parts/content', 'none' );
			}
		?>
	</div>
</div>
<?php get_footer(	); ?>